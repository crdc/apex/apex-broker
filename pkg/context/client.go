package context

import (
	"gitlab.com/crdc/apex/go-apex/api"
)

// ConnectClient connects a broker service client at the configured endpoint
func ConnectClient(config *Config) (*api.Client, error) {
	client, err := api.NewClient(config.Broker.ClientEndpoint)
	if err != nil {
		return nil, err
	}

	return client, nil
}
