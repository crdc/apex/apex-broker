package context

import (
	"fmt"
	"strings"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/viper"
)

type broker struct {
	Endpoint          string `mapstructure:"endpoint"`
	ClientEndpoint    string `mapstructure:"client-endpoint"`
	HeartbeatLiveness int    `mapstructure:"heartbeat-liveness"`
	HeartbeatInterval int    `mapstructure:"heartbeat-interval"`
}

type configure struct {
	Host   string `mapstructure:"host"`
	Port   int    `mapstructure:"port"`
	UseTLS bool   `mapstructure:"tls"`
	CACert string `mapstructure:"cacert"`
	Cert   string `mapstructure:"cert"`
	Key    string `mapstructure:"key"`
}

type unit struct {
	Endpoint string `mapstructure:"endpoint"`
	RPCBind  string `mapstructure:"rpc-bind"`
	RPCPort  int    `mapstructure:"rpc-port"`
}

type log struct {
	Debug bool   `mapstructure:"debug"`
	Level string `mapstructure:"level"`
}

type Config struct {
	App       string          `mapstructure:"app"`
	Broker    broker          `mapstructure:"broker"`
	Configure configure       `mapstructure:"configure"`
	Units     map[string]unit `mapstructure:"units"`
	Log       log             `mapstructure:"log"`
}

func LoadConfig() (*Config, error) {
	home, err := homedir.Dir()
	if err != nil {
		return nil, err
	}

	config := viper.New()
	config.SetConfigName("broker")
	config.AddConfigPath("/etc/plantd")
	config.AddConfigPath(".")
	config.AddConfigPath(fmt.Sprintf("%s/.config/plantd", home))

	err = config.ReadInConfig()
	if err != nil {
		return nil, err
	}

	config.SetEnvPrefix("PLANTD_BROKER")
	config.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	config.AutomaticEnv()

	var c Config

	err = config.Unmarshal(&c)
	if err != nil {
		return nil, err
	}

	return &c, nil
}
