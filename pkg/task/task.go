package task

import (
	"fmt"
	"net"

	"gitlab.com/crdc/apex/broker/pkg/context"
	rpc "gitlab.com/crdc/apex/broker/pkg/service"

	"gitlab.com/crdc/apex/go-apex/log"
	pb "gitlab.com/crdc/apex/go-apex/proto/v1"

	kitlog "github.com/go-kit/kit/log"
	"google.golang.org/grpc"
	//"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/reflection"
)

type Task struct {
	name     string
	endpoint *rpc.Endpoint
	config   *context.Config
	logger   *kitlog.Logger
}

func NewTask(name string, endpoint *rpc.Endpoint, config *context.Config, logger *kitlog.Logger) (task *Task, err error) {
	// Initialize service client / task
	task = &Task{
		name,
		endpoint,
		config,
		logger,
	}

	return task, nil
}

func (t *Task) Start() error {
	// Create the channel to listen on
	addr := fmt.Sprintf("%s:%d", t.config.Units[t.name].RPCBind, t.config.Units[t.name].RPCPort)
	log.Debug(t.logger, "msg", "bind task", "endpoint", addr)
	lis, err := net.Listen("tcp", addr)
	if err != nil {
		log.Error(t.logger, "msg", "failed to listen", "err", err)
		return err
	}

	var opts []grpc.ServerOption

	// TODO: add TLS support
	/*
	 *if t.config.Units[t.name].UseTLS {
	 *    log.Info(t.logger, "using TLS credentials")
	 *}
	 */

	log.Debug(t.logger, "msg", "start gRPC server", "task", t.name)

	// Start the gRPC endpoint
	srv := grpc.NewServer(opts...)
	pb.RegisterEndpointServer(srv, t.endpoint)
	reflection.Register(srv)
	if err := srv.Serve(lis); err != nil {
		log.Error(t.logger, "msg", "failed to serve", "err", err)
		return err
	}

	return nil
}
