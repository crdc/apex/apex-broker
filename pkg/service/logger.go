package service

import (
	"gitlab.com/crdc/apex/broker/pkg/context"

	"gitlab.com/crdc/apex/go-apex/log"

	kitlog "github.com/go-kit/kit/log"
)

// XXX: Maybe this should just GetLogger from go-apex, need to test

func NewLogger(config *context.Config) *kitlog.Logger {
	logger := log.GetLogger()
	l := kitlog.With(*logger, "service", config.App)
	logger = &l
	log.SetLevel(logger, config.Log.Level)

	return logger
}
