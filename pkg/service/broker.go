package service

import (
	"fmt"
	"runtime"
	"time"

	"gitlab.com/crdc/apex/go-apex/api"
	"gitlab.com/crdc/apex/go-apex/log"
	"gitlab.com/crdc/apex/go-apex/util"

	kitlog "github.com/go-kit/kit/log"
	zmq "github.com/pebbe/zmq4"
)

// TODO: get these from config data
const (
	HEARTBEAT_LIVENESS = 3                       // 3-5 is reasonable
	HEARTBEAT_INTERVAL = 2500 * time.Millisecond // msecs
	HEARTBEAT_EXPIRY   = HEARTBEAT_INTERVAL * HEARTBEAT_LIVENESS
)

// The broker class defines a single broker instance:

type Broker struct {
	Socket      *zmq.Socket         // Socket for clients & workers
	endpoint    string              // Broker binds to this endpoint
	services    map[string]*Service // Hash of known services
	workers     map[string]*Worker  // Hash of known workers
	Waiting     []*Worker           // List of waiting workers
	HeartbeatAt time.Time           // When to send HEARTBEAT
	logger      *kitlog.Logger
}

// The service class defines a single service instance:

type Service struct {
	broker   *Broker    // Broker instance
	name     string     // Service name
	requests [][]string // List of client requests
	waiting  []*Worker  // List of waiting workers
}

// The worker class defines a single worker, idle or active:

type Worker struct {
	broker    *Broker   // Broker instance
	id_string string    // Identity of worker as string
	identity  string    // Identity frame for routing
	service   *Service  // Owning service, if known
	expiry    time.Time // Expires at unless heartbeat
	logger    *kitlog.Logger
}

// Here are the constructor and destructor for the broker:

func NewBroker(logger *kitlog.Logger) (broker *Broker, err error) {

	// Initialize broker state
	broker = &Broker{
		services:    make(map[string]*Service),
		workers:     make(map[string]*Worker),
		Waiting:     make([]*Worker, 0),
		HeartbeatAt: time.Now().Add(HEARTBEAT_INTERVAL),
		logger:      logger,
	}
	broker.Socket, err = zmq.NewSocket(zmq.ROUTER)

	broker.Socket.SetRcvhwm(500000) // or example mdclient2 won't work

	runtime.SetFinalizer(broker, (*Broker).Close)
	return
}

func (b *Broker) Close() (err error) {
	if b.Socket != nil {
		err = b.Socket.Close()
		b.Socket = nil
	}
	return
}

// The bind method binds the broker instance to an endpoint. We can call
// this multiple times. Note that MDP uses a single socket for both clients
// and workers:

func (b *Broker) Bind(endpoint string) (err error) {
	err = b.Socket.Bind(endpoint)
	if err != nil {
		log.Error(b.logger, "msg", "MDP broker/0.2.0 failed to bind at", "endpoint", endpoint)
		return
	}
	log.Info(b.logger, "msg", "MDP broker/0.2.0 is active at", "endpoint", endpoint)
	return
}

// Runner for the service

func (b *Broker) Run(done chan bool) {
	poller := zmq.NewPoller()
	poller.Add(b.Socket, zmq.POLLIN)

	log.Info(b.logger, "msg", "Starting broker...")
	for {
		polled, err := poller.Poll(HEARTBEAT_INTERVAL)
		if err != nil {
			break // Interrupted
		}

		// Process next input message, if any
		if len(polled) > 0 {
			msg, err := b.Socket.RecvMessage(0)
			if err != nil {
				break // Interrupted
			}
			log.Debug(b.logger, "msg", "received", "body", msg)
			sender, msg := util.PopStr(msg)
			_, msg = util.PopStr(msg)
			header, msg := util.PopStr(msg)

			switch header {
			case api.MDPC_CLIENT:
				b.ClientMsg(sender, msg)
			case api.MDPW_WORKER:
				b.WorkerMsg(sender, msg)
			default:
				log.Warn(b.logger, "msg", "invalid message", "body", msg)
			}
		}

		// Disconnect and delete any expired workers
		// Send heartbeats to idle workers if needed
		if time.Now().After(b.HeartbeatAt) {
			b.Purge()
			for _, worker := range b.Waiting {
				worker.Send(api.MDPW_HEARTBEAT, "", []string{})
			}
			b.HeartbeatAt = time.Now().Add(HEARTBEAT_INTERVAL)
		}
	}

	done <- true
}

// The WorkerMsg method processes one READY, REPLY, HEARTBEAT or
// DISCONNECT message sent to the broker by a worker:

func (b *Broker) WorkerMsg(sender string, msg []string) {
	// At least, command
	if len(msg) == 0 {
		log.Error(b.logger, "len(msg) == 0")
	}

	command, msg := util.PopStr(msg)
	id_string := fmt.Sprintf("%q", sender)
	_, worker_ready := b.workers[id_string]
	worker := b.workerRequire(sender, b.logger)

	switch command {
	case api.MDPW_READY:
		if worker_ready { // Not first command in session
			worker.Delete(true)
		} else if len(sender) >= 4 /*  Reserved service name */ && sender[:4] == "mmi." {
			worker.Delete(true)
		} else {
			// Attach worker to service and mark as idle
			worker.service = b.ServiceRequire(msg[0])
			worker.Waiting()
		}
	case api.MDPW_REPLY:
		if worker_ready {
			// Remove & save client return envelope and insert the
			// protocol header and service name, then rewrap envelope.
			client, msg := util.Unwrap(msg)
			b.Socket.SendMessage(client, "", api.MDPC_CLIENT, worker.service.name, msg)
			worker.Waiting()
		} else {
			worker.Delete(true)
		}
	case api.MDPW_HEARTBEAT:
		if worker_ready {
			worker.expiry = time.Now().Add(HEARTBEAT_EXPIRY)
		} else {
			worker.Delete(true)
		}
	case api.MDPW_DISCONNECT:
		worker.Delete(false)
	default:
		log.Error(b.logger, "invalid input message %q\n", msg)
	}
}

// Process a request coming from a client. We implement MMI requests
// directly here (at present, we implement only the mmi.service request):

func (b *Broker) ClientMsg(sender string, msg []string) {
	// Service name + body
	if len(msg) < 2 {
		log.Error(b.logger, "msg", "len(msg) < 2")
	}

	service_frame, msg := util.PopStr(msg)
	service := b.ServiceRequire(service_frame)

	// Set reply return identity to client sender
	m := []string{sender, ""}
	msg = append(m, msg...)

	// If we got a MMI service request, process that internally
	if len(service_frame) >= 4 && service_frame[:4] == "mmi." {
		var return_code string
		if service_frame == "mmi.service" {
			name := msg[len(msg)-1]
			service, ok := b.services[name]
			if ok && len(service.waiting) > 0 {
				return_code = "200"
			} else {
				return_code = "404"
			}
		} else {
			return_code = "501"
		}

		msg[len(msg)-1] = return_code

		// Remove & save client return envelope and insert the
		// protocol header and service name, then rewrap envelope.
		client, msg := util.Unwrap(msg)
		b.Socket.SendMessage(client, "", api.MDPC_CLIENT, service_frame, msg)
	} else {
		// Else dispatch the message to the requested service
		service.Dispatch(msg)
	}
}

// The purge method deletes any idle workers that haven't pinged us in a
// while. We hold workers from oldest to most recent, so we can stop
// scanning whenever we find a live worker. This means we'll mainly stop
// at the first worker, which is essential when we have large numbers of
// workers (since we call this method in our critical path):

func (b *Broker) Purge() {
	now := time.Now()
	for len(b.Waiting) > 0 {
		if b.Waiting[0].expiry.After(now) {
			break // Worker is alive, we're done here
		}
		log.Debug(b.logger, "msg", "deleting expired worker", "worker", b.Waiting[0].id_string)
		b.Waiting[0].Delete(false)
	}
}

// Here is the implementation of the methods that work on a service:

// Lazy constructor that locates a service by name, or creates a new
// service if there is no service already with that name.

func (b *Broker) ServiceRequire(service_frame string) (service *Service) {
	name := service_frame
	service, ok := b.services[name]
	if !ok {
		service = &Service{
			broker:   b,
			name:     name,
			requests: make([][]string, 0),
			waiting:  make([]*Worker, 0),
		}
		b.services[name] = service
		log.Debug(b.logger, "msg", "added service", "name", name)
	}
	return
}

// The dispatch method sends requests to waiting workers:

func (s *Service) Dispatch(msg []string) {

	if len(msg) > 0 {
		// Queue message if any
		s.requests = append(s.requests, msg)
	}

	s.broker.Purge()
	for len(s.waiting) > 0 && len(s.requests) > 0 {
		var worker *Worker
		worker, s.waiting = popWorker(s.waiting)
		s.broker.Waiting = delWorker(s.broker.Waiting, worker)
		msg, s.requests = util.PopMsg(s.requests)
		worker.Send(api.MDPW_REQUEST, "", msg)
	}
}

// Here is the implementation of the methods that work on a worker:

// Lazy constructor that locates a worker by identity, or creates a new
// worker if there is no worker already with that identity.

func (b *Broker) workerRequire(identity string, logger *kitlog.Logger) (worker *Worker) {

	// b.workers is keyed off worker identity
	id_string := fmt.Sprintf("%q", identity)
	worker, ok := b.workers[id_string]
	if !ok {
		worker = &Worker{
			broker:    b,
			id_string: id_string,
			identity:  identity,
			logger:    logger,
		}
		b.workers[id_string] = worker
		log.Debug(b.logger, "msg", "registering new worker", "id", id_string)
	}
	return
}

// The delete method deletes the current worker.

func (w *Worker) Delete(disconnect bool) {
	if disconnect {
		w.Send(api.MDPW_DISCONNECT, "", []string{})
	}

	if w.service != nil {
		w.service.waiting = delWorker(w.service.waiting, w)
	}
	w.broker.Waiting = delWorker(w.broker.Waiting, w)
	delete(w.broker.workers, w.id_string)
}

// The send method formats and sends a command to a worker. The caller may
// also provide a command option, and a message payload:

func (w *Worker) Send(command, option string, msg []string) (err error) {
	n := 4
	if option != "" {
		n++
	}
	m := make([]string, n, n+len(msg))
	m = append(m, msg...)

	// Stack protocol envelope to start of message
	if option != "" {
		m[4] = option
	}
	m[3] = command
	m[2] = api.MDPW_WORKER

	// Stack routing envelope to start of message
	m[1] = ""
	m[0] = w.identity

	log.Debug(w.logger, "msg", "send", "command", api.MDPS_COMMANDS[command], "worker", m)
	_, err = w.broker.Socket.SendMessage(m)
	return
}

// This worker is now waiting for work

func (w *Worker) Waiting() {
	// Queue to broker and service waiting lists
	w.broker.Waiting = append(w.broker.Waiting, w)
	w.service.waiting = append(w.service.waiting, w)
	w.expiry = time.Now().Add(HEARTBEAT_EXPIRY)
	w.service.Dispatch([]string{})
}
