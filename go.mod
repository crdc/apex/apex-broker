module gitlab.com/crdc/apex/broker

require (
	github.com/go-kit/kit v0.8.0
	github.com/go-stack/stack v1.8.0 // indirect
	github.com/golang/protobuf v1.3.1
	github.com/mitchellh/go-homedir v1.0.0
	github.com/pebbe/zmq4 v1.0.0
	github.com/spf13/viper v1.3.1
	gitlab.com/crdc/apex/go-apex v0.1.20
	golang.org/x/net v0.0.0
	golang.org/x/sys v0.0.0 // indirect
	google.golang.org/grpc v1.19.0
)

replace golang.org/x/sys v0.0.0 => github.com/golang/sys v0.0.0-20190311152110-c8c8c57fd1e1

replace golang.org/x/net v0.0.0 => github.com/golang/net v0.0.0-20190311183353-d8887717615a
