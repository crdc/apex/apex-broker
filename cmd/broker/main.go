//
// Apex message broker.
// Implements the Majordomo Protocol as defined in
// http://rfc.zeromq.org/spec:7 and http://rfc.zeromq.org/spec:8.
//
package main

import (
	"fmt"
	"os"
	"regexp"

	gcontext "gitlab.com/crdc/apex/broker/pkg/context"
	"gitlab.com/crdc/apex/broker/pkg/service"
	"gitlab.com/crdc/apex/broker/pkg/task"

	"gitlab.com/crdc/apex/go-apex/api"
	"gitlab.com/crdc/apex/go-apex/log"

	"golang.org/x/net/context"
)

// Handles gRPC requests and ZeroMQ responses for Apex plant services.
// Handles ZeroMQ requests and gRPC responses for:
// - configure
// - experiment

func main() {
	if len(os.Args) > 1 {
		r, _ := regexp.Compile("^-v$|^(-{2})?version$")
		if r.Match([]byte(os.Args[1])) {
			fmt.Println(VERSION)
		}
		os.Exit(0)
	}

	// TODO: add gRPC ConfigureEndpointClient
	// TODO: rename context clients as a RPC relay

	config, err := gcontext.LoadConfig()
	if err != nil {
		fmt.Errorf("configuration error: %s", err)
		os.Exit(1)
	}
	logger := service.NewLogger(config)

	var clients map[string]*api.Client = make(map[string]*api.Client)
	var endpoints map[string]*service.Endpoint = make(map[string]*service.Endpoint)
	var tasks map[string]*task.Task = make(map[string]*task.Task)

	for name, unit := range config.Units {
		log.Info(logger, "msg", "adding service unit", "unit", name, "endpoint", unit.Endpoint)
		clients[name], err = gcontext.ConnectClient(config)
		if err != nil {
			log.Error(logger, "msg", "failed to create mdp client", "unit", name, "err", err)
		}
		// gRPC services
		endpoints[name] = service.NewEndpoint(name, clients[name], logger)
		tasks[name], err = task.NewTask(name, endpoints[name], config, logger)
		if err != nil {
			log.Error(logger, "msg", "failed to create task", "unit", name, "err", err)
		}
	}

	/*
	 *configureClient, err := gcontext.ConfigureClient(config)
	 *if err != nil {
	 *    log.Error(logger, "msg", "failed to create configure client", "err", err)
	 *}
	 */

	ctx := context.Background()

	// Include services in context
	ctx = context.WithValue(ctx, "config", config)
	ctx = context.WithValue(ctx, "log", logger)
	// XXX: not sure if the service or the task makes the most sense here
	for unit, endpoint := range endpoints {
		ctx = context.WithValue(ctx, unit, endpoint)
	}

	log.Info(logger, "msg", "starting tasks")
	for _, task := range tasks {
		go task.Start()
	}

	broker, _ := service.NewBroker(logger)
	broker.Bind(config.Broker.Endpoint)

	done := make(chan bool, 1)
	go broker.Run(done)

	// TODO: send this channel to the tasks also and close connections on done
	<-done

	log.Info(logger, "msg", "broker shutting down...")
}
