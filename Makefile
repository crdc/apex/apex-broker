PROJECT=broker
REGISTRY=registry.gitlab.com/crdc/apex/apex-$(PROJECT)
DESTDIR=/usr
CONFDIR=/etc
SYSTEMDDIR=/lib/systemd

M := $(shell printf "\033[34;1m▶\033[0m")
TAG := $(shell git describe --all | sed -e's/.*\///g')

all: build

lint: ; $(info $M Linting the files)
	@./tools/checks lint

test: ; $(info $M Running unittests)
	@./tools/checks test

race: ; $(info $M Running data race detector)
	@./tools/checks race

msan: ; $(info $M Running memory sanitizer)
	@./tools/checks msan

coverage: ; $(info $M Generating global code coverage report)
	@./tools/coverage

coverhtml: ; $(info $M Generating global code coverage report in HTML)
	@./tools/coverage html

build: ; $(info $(M) Building project...)
	@./tools/build -b

image: ; $(info $(M) Building application image...)
	@docker build -t $(REGISTRY) -f ./build/Dockerfile .

container: image ; $(info $(M) Running application container...)
	@docker run -p 4041-4045:4041-4045 -p 7200:7200 $(REGISTRY):latest

# FIXME: this assumes os/arch, consider using build output
install: ; $(info $(M) Installing plantd $(PROJECT) service...)
	@install -Dm 755 bin/$(PROJECT)-$(TAG)-linux-amd64 "$(DESTDIR)/bin/plantd-$(PROJECT)"
	@install -Dm 644 README.md "$(DESTDIR)/share/doc/plantd/$(PROJECT)/README"
	@install -Dm 644 LICENSE "$(DESTDIR)/share/licenses/plantd/$(PROJECT)/COPYING"
	@mkdir -p "$(CONFDIR)/plantd"
	@mkdir -p /run/plantd
	@install -Dm 644 configs/$(PROJECT).yaml "$(CONFDIR)/plantd/$(PROJECT).yaml"
	@install -Dm 644 init/plantd-$(PROJECT).service "$(SYSTEMDDIR)/system/plantd-$(PROJECT).service"
	@systemctl daemon-reload

uninstall: ; $(info $(M) Uninstalling plantd $(PROJECT) service...)
	@rm $(DESTDIR)/bin/plantd-$(PROJECT)

clean: ; $(info $(M) Removing generated files... )
	@rm -rf bin/

.PHONY: all lint test race msan coverage coverhtml build image container install uninstall clean
